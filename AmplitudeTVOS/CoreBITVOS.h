//
//  CoreBITVOS.h
//  CoreBITVOS
//
//  Created by Daniel Jih on 1/17/17.
//  Copyright © 2017 CoreBI. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreBITVOS.
FOUNDATION_EXPORT double AmplitudeTVOSVersionNumber;

//! Project version string for CoreBITVOS.
FOUNDATION_EXPORT const unsigned char CoreBITVOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreBITVOS/PublicHeader.h>


