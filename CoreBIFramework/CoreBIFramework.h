#import <UIKit/UIKit.h>

//! Project version number for CoreBI.
FOUNDATION_EXPORT double CoreBIVersionNumber;

//! Project version string for CoreBI.
FOUNDATION_EXPORT const unsigned char CoreBIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreBI/PublicHeader.h>

#import "CoreBI/CoreBIARCMacros.h"
#import "CoreBI/CoreBIConstants.h"
#import "CoreBI/CoreBIDatabaseHelper.h"
#import "CoreBI/CoreBIDeviceInfo.h"
#import "CoreBI/CoreBIIdentify.h"
#import "CoreBI/CoreBI.h"
#import "CoreBI/CoreBILocationManagerDelegate.h"
#import "CoreBI/CoreBIRevenue.h"
#import "CoreBI/CoreBIURLConnection.h"
#import "CoreBI/CoreBIUtils.h"
