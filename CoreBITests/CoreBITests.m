//
//  CoreBITests.m
//  CoreBI
//
//  Copyright (c) 2015 CoreBI. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import <OCMock/OCMock.h>
#import "CoreBI.h"
#import "CoreBIConstants.h"
#import "CoreBI+Test.h"
#import "BaseTestCase.h"
#import "CoreBIDeviceInfo.h"
#import "CoreBIARCMacros.h"
#import "CoreBIUtils.h"

// expose private methods for unit testing
@interface CoreBI (Tests)
- (NSDictionary *)mergeEventsAndIdentifys:(NSMutableArray *)events identifys:(NSMutableArray *)identifys numEvents:(long)numEvents;

- (id)truncate:(id)obj;

- (long long)getNextSequenceNumber;
@end

@interface CoreBITests : BaseTestCase

@end

@implementation CoreBITests {
    id _connectionMock;
    int _connectionCallCount;
}

- (void)setUp {
    [super setUp];
    _connectionMock = [OCMockObject mockForClass:NSURLConnection.class];
    _connectionCallCount = 0;
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : apiKey];
}

- (void)tearDown {
    [_connectionMock stopMocking];
}

- (void)setupAsyncResponse:(id)connectionMock response:(NSMutableDictionary *)serverResponse {
    [[[connectionMock expect] andDo:^(NSInvocation *invocation) {
        _connectionCallCount++;
        void (^handler)(NSURLResponse *, NSData *, NSError *);
        [invocation getArgument:&handler atIndex:4];
        handler(serverResponse[@"response"], serverResponse[@"data"], serverResponse[@"error"]);
    }] sendAsynchronousRequest:OCMOCK_ANY queue:OCMOCK_ANY completionHandler:OCMOCK_ANY];
}

- (void)testInstanceWithName {
    CoreBI *a = [CoreBI instance];
    CoreBI *b = [CoreBI instanceWithName:@""];
    CoreBI *c = [CoreBI instanceWithName:nil];
    CoreBI *e = [CoreBI instanceWithName:kRKMDefaultInstance];
    CoreBI *f = [CoreBI instanceWithName:@"app1"];
    CoreBI *g = [CoreBI instanceWithName:@"app2"];

    XCTAssertEqual(a, b);
    XCTAssertEqual(b, c);
    XCTAssertEqual(c, e);
    XCTAssertEqual(e, a);
    XCTAssertEqual(e, [CoreBI instance]);
    XCTAssertNotEqual(e, f);
    XCTAssertEqual(f, [CoreBI instanceWithName:@"app1"]);
    XCTAssertNotEqual(f, g);
    XCTAssertEqual(g, [CoreBI instanceWithName:@"app2"]);
}

- (void)testInitWithInstanceName {
    CoreBI *a = [CoreBI instanceWithName:@"APP1"];
    [a flushQueueWithQueue:a.initializerQueue];
    XCTAssertEqualObjects(a.instanceName, @"app1");
    XCTAssertTrue([a.propertyListPath rangeOfString:@"io.corebi.plist_app1"].location != NSNotFound);

    CoreBI *b = [CoreBI instanceWithName:[kRKMDefaultInstance uppercaseString]];
    [b flushQueueWithQueue:b.initializerQueue];
    XCTAssertEqualObjects(b.instanceName, kRKMDefaultInstance);
    XCTAssertTrue([b.propertyListPath rangeOfString:@"io.corebi.plist"].location != NSNotFound);
    XCTAssertTrue([b.propertyListPath rangeOfString:@"io.corebi.plist_"].location == NSNotFound);
}

- (void)testInitializeLoadNilUserIdFromEventData {
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nil);
    [self.corebi logEvent:@"test"];
    [self.corebi flushQueue];
    NSDictionary *event = [self.corebi getLastEvent];
    XCTAssertEqual([((NSDictionary *) [event objectForKey:@"properties"]) objectForKey:@"_user"], nil);
}

- (void)testSeparateInstancesLogEventsSeparate {
    NSString *newInstance1 = @"newApp1";
    NSString *newApiKey1 = @"1234567890";
    NSString *newInstance2 = @"newApp2";
    NSString *newApiKey2 = @"0987654321";

    CoreBIDatabaseHelper *oldDbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    CoreBIDatabaseHelper *newDBHelper1 = [CoreBIDatabaseHelper getDatabaseHelper:newInstance1];
    CoreBIDatabaseHelper *newDBHelper2 = [CoreBIDatabaseHelper getDatabaseHelper:newInstance2];

    // reset databases
    [oldDbHelper resetDB:NO];
    [newDBHelper1 resetDB:NO];
    [newDBHelper2 resetDB:NO];

    // setup existing database file, init default instance
    [oldDbHelper insertOrReplaceKeyLongValue:@"sequence_number" value:[NSNumber numberWithLongLong:1000]];
    [oldDbHelper addEvent:@"{\"collection\":\"oldEvent\"}"];
    [oldDbHelper addIdentify:@"{\"collection\":\"$identify\"}"];
    [oldDbHelper addIdentify:@"{\"collection\":\"$identify\"}"];

    [[CoreBI instance] setDeviceId:@"oldDeviceId"];
    [[CoreBI instance] flushQueue];
    XCTAssertEqualObjects([oldDbHelper getValue:@"device_id"], @"oldDeviceId");
    XCTAssertEqualObjects([[CoreBI instance] getDeviceId], @"oldDeviceId");
    XCTAssertEqual([[CoreBI instance] getNextSequenceNumber], 1001);

    XCTAssertNil([newDBHelper1 getValue:@"device_id"]);
    XCTAssertNil([newDBHelper2 getValue:@"device_id"]);
    XCTAssertEqualObjects([oldDbHelper getLongValue:@"sequence_number"], [NSNumber numberWithLongLong:1001]);
    XCTAssertNil([newDBHelper1 getLongValue:@"sequence_number"]);
    XCTAssertNil([newDBHelper2 getLongValue:@"sequence_number"]);

    // init first new app and verify separate database
    [[CoreBI instanceWithName:newInstance1] initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : newApiKey1];
    [[CoreBI instanceWithName:newInstance1] flushQueue];
    XCTAssertNotEqualObjects([[CoreBI instanceWithName:newInstance1] getDeviceId], @"oldDeviceId");
    XCTAssertEqualObjects([[CoreBI instanceWithName:newInstance1] getDeviceId], [newDBHelper1 getValue:@"device_id"]);
    XCTAssertEqual([[CoreBI instanceWithName:newInstance1] getNextSequenceNumber], 1);
    XCTAssertEqual([newDBHelper1 getEventCount], 0);
    XCTAssertEqual([newDBHelper1 getIdentifyCount], 0);

    // init second new app and verify separate database
    [[CoreBI instanceWithName:newInstance2] initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : newApiKey2];
    [[CoreBI instanceWithName:newInstance2] flushQueue];
    XCTAssertNotEqualObjects([[CoreBI instanceWithName:newInstance2] getDeviceId], @"oldDeviceId");
    XCTAssertEqualObjects([[CoreBI instanceWithName:newInstance2] getDeviceId], [newDBHelper2 getValue:@"device_id"]);
    XCTAssertEqual([[CoreBI instanceWithName:newInstance2] getNextSequenceNumber], 1);
    XCTAssertEqual([newDBHelper2 getEventCount], 0);
    XCTAssertEqual([newDBHelper2 getIdentifyCount], 0);

    // verify old database still intact
    XCTAssertEqualObjects([oldDbHelper getValue:@"device_id"], @"oldDeviceId");
    XCTAssertEqualObjects([oldDbHelper getLongValue:@"sequence_number"], [NSNumber numberWithLongLong:1001]);
    XCTAssertEqual([oldDbHelper getEventCount], 1);
    XCTAssertEqual([oldDbHelper getIdentifyCount], 2);

    // verify both apps can modify database independently and not affect old database
    [[CoreBI instanceWithName:newInstance1] setDeviceId:@"fakeDeviceId"];
    [[CoreBI instanceWithName:newInstance1] flushQueue];
    XCTAssertEqualObjects([newDBHelper1 getValue:@"device_id"], @"fakeDeviceId");
    XCTAssertNotEqualObjects([newDBHelper2 getValue:@"device_id"], @"fakeDeviceId");
    XCTAssertEqualObjects([oldDbHelper getValue:@"device_id"], @"oldDeviceId");
    [newDBHelper1 addIdentify:@"{\"collection\":\"$identify\"}"];
    XCTAssertEqual([newDBHelper1 getIdentifyCount], 1);
    XCTAssertEqual([newDBHelper2 getIdentifyCount], 0);
    XCTAssertEqual([oldDbHelper getIdentifyCount], 2);

    [[CoreBI instanceWithName:newInstance2] setDeviceId:@"brandNewDeviceId"];
    [[CoreBI instanceWithName:newInstance2] flushQueue];
    XCTAssertEqualObjects([newDBHelper1 getValue:@"device_id"], @"fakeDeviceId");
    XCTAssertEqualObjects([newDBHelper2 getValue:@"device_id"], @"brandNewDeviceId");
    XCTAssertEqualObjects([oldDbHelper getValue:@"device_id"], @"oldDeviceId");
    [newDBHelper2 addEvent:@"{\"collection\":\"testEvent2\"}"];
    [newDBHelper2 addEvent:@"{\"collection\":\"testEvent3\"}"];
    XCTAssertEqual([newDBHelper1 getEventCount], 0);
    XCTAssertEqual([newDBHelper2 getEventCount], 2);
    XCTAssertEqual([oldDbHelper getEventCount], 1);

    [newDBHelper1 deleteDB];
    [newDBHelper2 deleteDB];
}

- (void)testInitializeLoadUserIdFromEventData {
    NSString *instanceName = @"testInitialize";
    CoreBI *client = [CoreBI instanceWithName:instanceName];
    [client flushQueue];
    XCTAssertEqual([client userId], nil);

    NSString *testUserId = @"testUserId";
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper:instanceName];
    [dbHelper insertOrReplaceKeyValue:@"_user" value:testUserId];
    [client initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : apiKey];
    [client flushQueue];
    XCTAssertTrue([[client userId] isEqualToString:testUserId]);
}

- (void)testInitializeWithNilUserId {
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nil);

    NSString *nilUserId = nil;
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : apiKey userId:nilUserId];
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nilUserId);
    XCTAssertNil([[CoreBIDatabaseHelper getDatabaseHelper] getValue:@"_user"]);
}

- (void)testInitializeWithUserId {
    NSString *instanceName = @"testInitializeWithUserId";
    CoreBI *client = [CoreBI instanceWithName:instanceName];
    [client flushQueue];
    XCTAssertEqual([client userId], nil);

    NSString *testUserId = @"testUserId";
    [client initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : apiKey userId:testUserId];
    [client flushQueue];
    XCTAssertEqual([client userId], testUserId);
}

- (void)testSkipReinitialization {
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nil);

    NSString *testUserId = @"testUserId";
    [self.corebi initializeApiKey:[NSURL URLWithString:@"hhttp://127.0.0.1:9998"] : apiKey userId:testUserId];
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nil);
}

- (void)testClearUserId {
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nil);

    NSString *testUserId = @"testUserId";
    [self.corebi setUserId:testUserId];
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], testUserId);
    [self.corebi logEvent:@"test"];
    [self.corebi flushQueue];
    NSDictionary *event1 = [self.corebi getLastEvent];
    XCTAssert([[((NSDictionary *) [event1 objectForKey:@"properties"]) objectForKey:@"_user"] isEqualToString:testUserId]);

    NSString *nilUserId = nil;
    [self.corebi setUserId:nilUserId];
    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi userId], nilUserId);
    [self.corebi logEvent:@"test"];
    [self.corebi flushQueue];
    NSDictionary *event2 = [self.corebi getLastEvent];
    XCTAssertEqual([((NSDictionary *) [event2 objectForKey:@"properties"]) objectForKey:@"_user"], nilUserId);
}

- (void)testRequestTooLargeBackoffLogic {
    [self.corebi setEventUploadThreshold:2];
    NSMutableDictionary *serverResponse = [NSMutableDictionary dictionaryWithDictionary:
            @{@"response": [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@"/"] statusCode:413 HTTPVersion:nil headerFields:@{}],
                    @"data": [@"response" dataUsingEncoding:NSUTF8StringEncoding]
            }];

    // 413 error force backoff with 2 events --> new upload limit will be 1
    [self setupAsyncResponse:_connectionMock response:serverResponse];
    [self.corebi logEvent:@"test"];
    [self.corebi logEvent:@"test"];
    [self.corebi flushQueue];

    // after first 413, the backoffupload batch size should now be 1
    XCTAssertTrue(self.corebi.backoffUpload);
    XCTAssertEqual(self.corebi.backoffUploadBatchSize, 1);
    XCTAssertEqual(_connectionCallCount, 1);
}

- (void)testRequestTooLargeBackoffRemoveEvent {
    [self.corebi setEventUploadThreshold:1];
    NSMutableDictionary *serverResponse = [NSMutableDictionary dictionaryWithDictionary:
            @{@"response": [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@"/"] statusCode:413 HTTPVersion:nil headerFields:@{}],
                    @"data": [@"response" dataUsingEncoding:NSUTF8StringEncoding]
            }];

    // 413 error force backoff with 1 events --> should drop the event
    [self setupAsyncResponse:_connectionMock response:serverResponse];
    [self.corebi logEvent:@"test"];
    [self.corebi flushQueue];

    // after first 413, the backoffupload batch size should now be 1
    XCTAssertTrue(self.corebi.backoffUpload);
    XCTAssertEqual(self.corebi.backoffUploadBatchSize, 1);
    XCTAssertEqual(_connectionCallCount, 1);
    XCTAssertEqual([self.databaseHelper getEventCount], 0);
}

- (void)testUUIDInEvent {
    [self.corebi setEventUploadThreshold:5];
    [self.corebi logEvent:@"event1"];
    [self.corebi logEvent:@"event2"];
    [self.corebi flushQueue];

    XCTAssertEqual([self.corebi queuedEventCount], 2);
    NSArray *events = [[CoreBIDatabaseHelper getDatabaseHelper] getEvents:-1 limit:-1];
    XCTAssertEqual(2, [[events[1] objectForKey:@"event_id"] intValue]);

    id event1Uuid = [((NSDictionary *) [events[0] objectForKey:@"api"]) objectForKey:@"uuid"];
    id event2Uuid = [((NSDictionary *) [events[1] objectForKey:@"api"]) objectForKey:@"uuid"];

    XCTAssertNotNil(event1Uuid);
    XCTAssertNotNil(event2Uuid);
    XCTAssertNotEqual(event1Uuid, event2Uuid);
}

- (void)testIdentify {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi setEventUploadThreshold:2];

    CoreBIIdentify *identify = [[CoreBIIdentify identify] set:@"key1" value:@"value1"];
    [self.corebi identify:identify];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 1);
    XCTAssertEqual([dbHelper getTotalEventCount], 1);

    NSDictionary *operations = [NSDictionary dictionaryWithObject:@"value1" forKey:@"key1"];
    NSDictionary *event = [self.corebi getLastIdentify];
    XCTAssertEqualObjects([((NSDictionary *) [event objectForKey:@"properties"]) objectForKey:@"$set"], operations);

    NSMutableDictionary *serverResponse = [NSMutableDictionary dictionaryWithDictionary:
            @{@"response": [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@"/"] statusCode:200 HTTPVersion:nil headerFields:@{}],
                    @"data": [@"success" dataUsingEncoding:NSUTF8StringEncoding]
            }];
    [self setupAsyncResponse:_connectionMock response:serverResponse];
    CoreBIIdentify *identify2 = [[[CoreBIIdentify alloc] init] set:@"key2" value:@"value2"];
    [self.corebi identify:identify2];
    SAFE_ARC_RELEASE(identify2);
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 0);
    XCTAssertEqual([dbHelper getTotalEventCount], 0);
}

- (void)testLogRevenue {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];

    // ignore invalid revenue objects
    [self.corebi logRevenue:nil];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], 0);

    [self.corebi logRevenue:[CoreBIRevenue revenue]];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], 0);

    // log valid revenue object
    NSNumber *price = [NSNumber numberWithDouble:15.99];
    NSInteger quantity = 15;
    NSString *productId = @"testProductId";
    NSString *revenueType = @"testRevenueType";
    NSDictionary *props = [NSDictionary dictionaryWithObject:@"San Francisco" forKey:@"city"];
    CoreBIRevenue *revenue = [[[[CoreBIRevenue revenue] setProductIdentifier:productId] setPrice:price] setQuantity:quantity];
    [[revenue setRevenueType:revenueType] setEventProperties:props];

    [self.corebi logRevenue:revenue];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], 1);

    NSDictionary *event = [self.corebi getLastEvent];
    XCTAssertEqualObjects([event objectForKey:@"collection"], @"revenue_amount");

    NSDictionary *dict = [event objectForKey:@"properties"];
    XCTAssertEqualObjects([dict objectForKey:@"_product_id"], productId);
    XCTAssertEqualObjects([dict objectForKey:@"_price"], price);
    XCTAssertEqualObjects([dict objectForKey:@"_quantity"], [NSNumber numberWithInteger:quantity]);
    XCTAssertEqualObjects([dict objectForKey:@"_revenue_type"], revenueType);
    XCTAssertEqualObjects([dict objectForKey:@"city"], @"San Francisco");
}

- (void)test {
    [self.corebi setEventUploadThreshold:1];
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    [properties setObject:@"some event description" forKey:@"description"];
    [properties setObject:@"green" forKey:@"color"];
    [properties setObject:@"productIdentifier" forKey:@"_product_id"];
    [properties setObject:[NSNumber numberWithDouble:10.99] forKey:@"_price"];
    [properties setObject:[NSNumber numberWithInt:2] forKey:@"_quantity"];
    [self.corebi logEvent:@"Completed Purchase" withEventProperties:properties];
}

- (void)testMergeEventsAndIdentifys {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi setEventUploadThreshold:7];
    NSMutableDictionary *serverResponse = [NSMutableDictionary dictionaryWithDictionary:
            @{@"response": [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@"/"] statusCode:200 HTTPVersion:nil headerFields:@{}],
                    @"data": [@"success" dataUsingEncoding:NSUTF8StringEncoding]
            }];
    [self setupAsyncResponse:_connectionMock response:serverResponse];

    [self.corebi logEvent:@"test_event1"];
    [self.corebi identify:[[CoreBIIdentify identify] add:@"photoCount" value:[NSNumber numberWithInt:1]]];
    [self.corebi logEvent:@"test_event2"];
    [self.corebi logEvent:@"test_event3"];
    [self.corebi logEvent:@"test_event4"];
    [self.corebi identify:[[CoreBIIdentify identify] set:@"gender" value:@"male"]];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 4);
    XCTAssertEqual([dbHelper getIdentifyCount], 2);
    XCTAssertEqual([dbHelper getTotalEventCount], 6);

    // verify merging
    NSMutableArray *events = [dbHelper getEvents:-1 limit:-1];
    NSMutableArray *identifys = [dbHelper getIdentifys:-1 limit:-1];
    NSDictionary *merged = [self.corebi mergeEventsAndIdentifys:events identifys:identifys numEvents:[dbHelper getTotalEventCount]];
    NSArray *mergedEvents = [merged objectForKey:@"events"];

    XCTAssertEqual(4, [[merged objectForKey:@"max_event_id"] intValue]);
    XCTAssertEqual(2, [[merged objectForKey:@"max_identify_id"] intValue]);
    XCTAssertEqual(6, [mergedEvents count]);

    XCTAssertEqualObjects([mergedEvents[0] objectForKey:@"collection"], @"test_event1");
    XCTAssertEqual([[mergedEvents[0] objectForKey:@"event_id"] intValue], 1);

    XCTAssertEqualObjects([mergedEvents[1] objectForKey:@"collection"], @"test_event2");
    XCTAssertEqual([[mergedEvents[1] objectForKey:@"event_id"] intValue], 2);

    XCTAssertEqualObjects([mergedEvents[2] objectForKey:@"collection"], @"test_event3");
    XCTAssertEqual([[mergedEvents[2] objectForKey:@"event_id"] intValue], 3);

    XCTAssertEqualObjects([mergedEvents[3] objectForKey:@"collection"], @"test_event4");
    XCTAssertEqual([[mergedEvents[3] objectForKey:@"event_id"] intValue], 4);

    XCTAssertEqualObjects([mergedEvents[4] objectForKey:@"collection"], @"$$user");
    XCTAssertEqual([[mergedEvents[4] objectForKey:@"event_id"] intValue], 1);
    XCTAssertTrue([self key:[mergedEvents[4] objectForKey:@"properties"]
       containsInDictionary:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:1] forKey:@"photoCount"] forKey:@"$add"]]);

    XCTAssertEqualObjects([mergedEvents[5] objectForKey:@"collection"], @"$$user");
    XCTAssertEqual([[mergedEvents[5] objectForKey:@"event_id"] intValue], 2);
    XCTAssertTrue([self key:[mergedEvents[5] objectForKey:@"properties"]
       containsInDictionary:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:@"male" forKey:@"gender"] forKey:@"$set"]]);

    [self.corebi identify:[[CoreBIIdentify identify] unset:@"karma"]];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 0);
    XCTAssertEqual([dbHelper getTotalEventCount], 0);
}

- (void)testMergeEventsBackwardsCompatible {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi identify:[[CoreBIIdentify identify] unset:@"key"]];
    [self.corebi logEvent:@"test_event"];
    [self.corebi flushQueue];

    NSMutableDictionary *event = [NSMutableDictionary dictionaryWithDictionary:[self.corebi getLastEvent]];
    long eventId = [[event objectForKey:@"event_id"] longValue];
    [dbHelper removeEvent:eventId];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:event options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [dbHelper addEvent:jsonString];
    SAFE_ARC_RELEASE(jsonString);

    NSMutableArray *events = [dbHelper getEvents:-1 limit:-1];
    NSMutableArray *identifys = [dbHelper getIdentifys:-1 limit:-1];
    NSDictionary *merged = [self.corebi mergeEventsAndIdentifys:events identifys:identifys numEvents:[dbHelper getTotalEventCount]];
    NSArray *mergedEvents = [merged objectForKey:@"events"];
    XCTAssertEqualObjects([mergedEvents[0] objectForKey:@"collection"], @"test_event");
    XCTAssertEqualObjects([mergedEvents[1] objectForKey:@"collection"], @"$$user");
}

- (void)testTruncateLongStrings {
    NSString *longString = [@"" stringByPaddingToLength:kRKMMaxStringLength * 2 withString:@"c" startingAtIndex:0];
    XCTAssertEqual([longString length], kRKMMaxStringLength * 2);
    NSString *truncatedString = [self.corebi truncate:longString];
    XCTAssertEqual([truncatedString length], kRKMMaxStringLength);
    XCTAssertEqualObjects(truncatedString, [@"" stringByPaddingToLength:kRKMMaxStringLength withString:@"c" startingAtIndex:0]);

    NSString *shortString = [@"" stringByPaddingToLength:kRKMMaxStringLength - 1 withString:@"c" startingAtIndex:0];
    XCTAssertEqual([shortString length], kRKMMaxStringLength - 1);
    truncatedString = [self.corebi truncate:shortString];
    XCTAssertEqual([truncatedString length], kRKMMaxStringLength - 1);
    XCTAssertEqualObjects(truncatedString, shortString);
}

- (void)testTruncateNullObjects {
    XCTAssertNil([self.corebi truncate:nil]);
}

- (void)testTruncateDictionary {
    NSString *longString = [@"" stringByPaddingToLength:kRKMMaxStringLength * 2 withString:@"c" startingAtIndex:0];
    NSString *truncString = [@"" stringByPaddingToLength:kRKMMaxStringLength withString:@"c" startingAtIndex:0];
    NSMutableDictionary *object = [NSMutableDictionary dictionary];
    [object setValue:[NSNumber numberWithInt:10] forKey:@"int value"];
    [object setValue:[NSNumber numberWithBool:NO] forKey:@"bool value"];
    [object setValue:longString forKey:@"long string"];
    [object setValue:[NSArray arrayWithObject:longString] forKey:@"array"];
    [object setValue:longString forKey:RKM_REVENUE_RECEIPT];

    object = [self.corebi truncate:object];
    XCTAssertEqual([[object objectForKey:@"int value"] intValue], 10);
    XCTAssertFalse([[object objectForKey:@"bool value"] boolValue]);
    XCTAssertEqual([[object objectForKey:@"long string"] length], kRKMMaxStringLength);
    XCTAssertEqual([[object objectForKey:@"array"] count], 1);
    XCTAssertEqualObjects([object objectForKey:@"array"][0], truncString);
    XCTAssertEqual([[object objectForKey:@"array"][0] length], kRKMMaxStringLength);

    // receipt field should not be truncated
    XCTAssertEqualObjects([object objectForKey:RKM_REVENUE_RECEIPT], longString);
}

- (void)testTruncateEventAndIdentify {
    NSString *longString = [@"" stringByPaddingToLength:kRKMMaxStringLength * 2 withString:@"c" startingAtIndex:0];
    NSString *truncString = [@"" stringByPaddingToLength:kRKMMaxStringLength withString:@"c" startingAtIndex:0];

    NSDictionary *props = [NSDictionary dictionaryWithObjectsAndKeys:longString, @"long_string", longString, RKM_REVENUE_RECEIPT, nil];
    [self.corebi logEvent:@"test" withEventProperties:props];
    [self.corebi identify:[[CoreBIIdentify identify] set:@"long_string" value:longString]];
    [self.corebi flushQueue];

    NSDictionary *event = [self.corebi getLastEvent];
    NSDictionary *expected = [NSDictionary dictionaryWithObjectsAndKeys:truncString, @"long_string", longString, RKM_REVENUE_RECEIPT, nil];
    XCTAssertEqualObjects([event objectForKey:@"collection"], @"test");
    XCTAssertTrue([self key:[event objectForKey:@"properties"] containsInDictionary:expected]);

    NSDictionary *identify = [self.corebi getLastIdentify];
    XCTAssertEqualObjects([identify objectForKey:@"collection"], @"$$user");
    XCTAssertTrue([self key:[identify objectForKey:@"properties"] containsInDictionary:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:truncString forKey:@"long_string"] forKey:@"$set"]]);
}

-(BOOL)key:(NSDictionary *)main containsInDictionary:(NSDictionary *)Dictionary
{
    for (NSString *keyStr in Dictionary) {
        if(![[Dictionary objectForKey:keyStr] isEqual:[main objectForKey:keyStr]]) {
            return false;
        }
    }
    return true;
}

- (void)testAutoIncrementSequenceNumber {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    int limit = 10;
    for (int i = 0; i < limit; i++) {
        XCTAssertEqual([self.corebi getNextSequenceNumber], i + 1);
        XCTAssertEqual([[dbHelper getLongValue:@"sequence_number"] intValue], i + 1);
    }
}

- (void)testSetOffline {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    NSMutableDictionary *serverResponse = [NSMutableDictionary dictionaryWithDictionary:
            @{@"response": [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@"/"] statusCode:200 HTTPVersion:nil headerFields:@{}],
                    @"data": [@"success" dataUsingEncoding:NSUTF8StringEncoding]
            }];
    [self setupAsyncResponse:_connectionMock response:serverResponse];

    [self.corebi setOffline:YES];
    [self.corebi logEvent:@"test"];
    [self.corebi logEvent:@"test"];
    [self.corebi identify:[[CoreBIIdentify identify] set:@"key" value:@"value"]];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 2);
    XCTAssertEqual([dbHelper getIdentifyCount], 1);
    XCTAssertEqual([dbHelper getTotalEventCount], 3);

    [self.corebi setOffline:NO];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 0);
    XCTAssertEqual([dbHelper getTotalEventCount], 0);
}

- (void)testSetOfflineTruncate {
    int eventMaxCount = 3;
    self.corebi.eventMaxCount = eventMaxCount;

    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    NSMutableDictionary *serverResponse = [NSMutableDictionary dictionaryWithDictionary:
            @{@"response": [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@"/"] statusCode:200 HTTPVersion:nil headerFields:@{}],
                    @"data": [@"success" dataUsingEncoding:NSUTF8StringEncoding]
            }];
    [self setupAsyncResponse:_connectionMock response:serverResponse];

    [self.corebi setOffline:YES];
    [self.corebi logEvent:@"test1"];
    [self.corebi logEvent:@"test2"];
    [self.corebi logEvent:@"test3"];
    [self.corebi identify:[[CoreBIIdentify identify] unset:@"key1"]];
    [self.corebi identify:[[CoreBIIdentify identify] unset:@"key2"]];
    [self.corebi identify:[[CoreBIIdentify identify] unset:@"key3"]];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 3);
    XCTAssertEqual([dbHelper getIdentifyCount], 3);
    XCTAssertEqual([dbHelper getTotalEventCount], 6);

    [self.corebi logEvent:@"test4"];
    [self.corebi identify:[[CoreBIIdentify identify] unset:@"key4"]];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 3);
    XCTAssertEqual([dbHelper getIdentifyCount], 3);
    XCTAssertEqual([dbHelper getTotalEventCount], 6);

    NSMutableArray *events = [dbHelper getEvents:-1 limit:-1];
    XCTAssertEqual([events count], 3);
    XCTAssertEqualObjects([events[0] objectForKey:@"collection"], @"test2");
    XCTAssertEqualObjects([events[1] objectForKey:@"collection"], @"test3");
    XCTAssertEqualObjects([events[2] objectForKey:@"collection"], @"test4");

    NSMutableArray *identifys = [dbHelper getIdentifys:-1 limit:-1];
    XCTAssertEqual([identifys count], 3);
    XCTAssertEqualObjects([[[identifys[0] objectForKey:@"properties"] objectForKey:@"$unset"] objectForKey:@"key2"], @"-");
    XCTAssertEqualObjects([[[identifys[1] objectForKey:@"properties"] objectForKey:@"$unset"] objectForKey:@"key3"], @"-");
    XCTAssertEqualObjects([[[identifys[2] objectForKey:@"properties"] objectForKey:@"$unset"] objectForKey:@"key4"], @"-");


    [self.corebi setOffline:NO];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 0);
    XCTAssertEqual([dbHelper getTotalEventCount], 0);
}

- (void)testTruncateEventsQueues {
    int eventMaxCount = 50;
    XCTAssertGreaterThanOrEqual(eventMaxCount, kRKMEventRemoveBatchSize);
    self.corebi.eventMaxCount = eventMaxCount;

    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi setOffline:YES];
    for (int i = 0; i < eventMaxCount; i++) {
        [self.corebi logEvent:@"test"];
    }
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], eventMaxCount);

    [self.corebi logEvent:@"test"];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], eventMaxCount - (eventMaxCount / 10) + 1);
}

- (void)testTruncateEventsQueuesWithOneEvent {
    int eventMaxCount = 1;
    self.corebi.eventMaxCount = eventMaxCount;

    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi logEvent:@"test1"];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], eventMaxCount);

    [self.corebi logEvent:@"test2"];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], eventMaxCount);

    NSDictionary *event = [self.corebi getLastEvent];
    XCTAssertEqualObjects([event objectForKey:@"collection"], @"test2");
}

- (void)testInvalidJSONEventProperties {
    NSURL *url = [NSURL URLWithString:@"https://corebi.io/"];
    NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:url, url, url, @"url", nil];
    [self.corebi logEvent:@"test" withEventProperties:properties];
    [self.corebi flushQueue];
    XCTAssertEqual([[CoreBIDatabaseHelper getDatabaseHelper] getEventCount], 1);
}

- (void)testClearUserProperties {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi setEventUploadThreshold:2];

    [self.corebi clearUserProperties];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 1);
    XCTAssertEqual([dbHelper getTotalEventCount], 1);

    NSDictionary *event = [self.corebi getLastIdentify];
    XCTAssertEqualObjects([event objectForKey:@"collection"], IDENTIFY_EVENT);
    XCTAssertEqualObjects([((NSDictionary *) [event objectForKey:@"properties"]) objectForKey:@"$clearAll"], @"-");
}

- (void)testUnarchiveEventsDict {
    NSString *archiveName = @"test_archive";
    NSDictionary *event = [NSDictionary dictionaryWithObject:@"test event" forKey:@"collection"];
    XCTAssertTrue([self.corebi archive:event toFile:archiveName]);

    NSDictionary *unarchived = [self.corebi unarchive:archiveName];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4) {
        XCTAssertEqualObjects(unarchived, event);
    } else {
        XCTAssertNil(unarchived);
    }
}

- (void)testBlockTooManyProperties {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];

    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    NSMutableDictionary *userProperties = [NSMutableDictionary dictionary];
    CoreBIIdentify *identify = [CoreBIIdentify identify];
    for (int i = 0; i < kRKMMaxPropertyKeys + 1; i++) {
        [eventProperties setObject:[NSNumber numberWithInt:i] forKey:[NSNumber numberWithInt:i]];
        [userProperties setObject:[NSNumber numberWithInt:i * 2] forKey:[NSNumber numberWithInt:i * 2]];
        [identify setOnce:[NSString stringWithFormat:@"%d", i] value:[NSNumber numberWithInt:i]];
    }

    // verify that setUserProperties ignores dict completely
    [self.corebi setUserProperties:userProperties];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getIdentifyCount], 0);

    // verify that event properties and user properties are scrubbed
    [self.corebi logEvent:@"test event" withEventProperties:eventProperties];
    [self.corebi identify:identify];
    [self.corebi flushQueue];

    XCTAssertEqual([dbHelper getEventCount], 1);

    XCTAssertEqual([dbHelper getIdentifyCount], 1);
    NSDictionary *identifyEvent = [self.corebi getLastIdentify];
    XCTAssertEqualObjects([((NSDictionary *) identifyEvent[@"properties"]) objectForKey:@"$setOnce"], [NSDictionary dictionary]);
}

- (void)testLogEventWithTimestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1000];
    NSNumber *timestamp = [NSNumber numberWithLongLong:[date timeIntervalSince1970]];

    [self.corebi logEvent:@"test" withEventProperties:nil withGroups:nil withTimestamp:timestamp outOfSession:NO];
    [self.corebi flushQueue];
    NSDictionary *event = [self.corebi getLastEvent];
    XCTAssertEqual(1000, [[((NSDictionary *) [event objectForKey:@"properties"]) objectForKey:@"_time"] longLongValue]);

    [self.corebi logEvent:@"test2" withEventProperties:nil withGroups:nil withLongLongTimestamp:2000 outOfSession:NO];
    [self.corebi flushQueue];
    event = [self.corebi getLastEvent];
    XCTAssertEqual(2000, [[((NSDictionary *) [event objectForKey:@"properties"]) objectForKey:@"_time"] longLongValue]);
}

- (void)testRegenerateDeviceId {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    [self.corebi flushQueue];
    NSString *oldDeviceId = [self.corebi getDeviceId];
    XCTAssertFalse([CoreBIUtils isEmptyString:oldDeviceId]);
    XCTAssertEqualObjects(oldDeviceId, [dbHelper getValue:@"device_id"]);

    [self.corebi regenerateDeviceId];
    [self.corebi flushQueue];
    NSString *newDeviceId = [self.corebi getDeviceId];
    XCTAssertNotEqualObjects(oldDeviceId, newDeviceId);
    XCTAssertEqualObjects(newDeviceId, [dbHelper getValue:@"device_id"]);
    XCTAssertTrue([newDeviceId hasSuffix:@"R"]);
}

@end
