//
//  BaseTestCase.m
//  CoreBI
//
//  Created by Allan on 3/11/15.
//  Copyright (c) 2015 CoreBI. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import <OCMock/OCMock.h>
#import "CoreBI.h"
#import "CoreBI+Test.h"
#import "BaseTestCase.h"
#import "CoreBIARCMacros.h"
#import "CoreBIDatabaseHelper.h"

NSString *const apiKey = @"000000";
NSString *const userId = @"userId";

@implementation BaseTestCase {
    id _archivedObj;
}

- (void)setUp {
    [super setUp];
    self.corebi = [CoreBI alloc];
    self.databaseHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    XCTAssertTrue([self.databaseHelper resetDB:NO]);

    [self.corebi init];
    self.corebi.sslPinningEnabled = NO;
}

- (void)tearDown {
    // Ensure all background operations are done
    [self.corebi flushQueueWithQueue:self.corebi.initializerQueue];
    [self.corebi flushQueue];
    SAFE_ARC_RELEASE(_corebi);
    SAFE_ARC_RELEASE(_databaseHelper);
    [super tearDown];
}

- (BOOL)archive:(id)rootObject toFile:(NSString *)path {
    _archivedObj = rootObject;
    return YES;
}

- (id)unarchive:(NSString *)path {
    return _archivedObj;
}

@end
