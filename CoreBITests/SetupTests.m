//
//  SessionTests.m
//  SessionTests
//
//  Created by Curtis on 9/24/14.
//  Copyright (c) 2014 CoreBI. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import <OCMock/OCMock.h>
#import "CoreBI.h"
#import "CoreBI+Test.h"
#import "BaseTestCase.h"
#import "CoreBIConstants.h"
#import "CoreBIUtils.h"

@interface SetupTests : BaseTestCase

@end

@implementation SetupTests { }

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testApiKeySet {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    XCTAssertEqual(self.corebi.apiKey, apiKey);
}

- (void)testDeviceIdSet {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    [self.corebi flushQueue];
    XCTAssertNotNil([self.corebi deviceId]);
    XCTAssertEqual([self.corebi deviceId].length, 36);
    XCTAssertEqualObjects([self.corebi deviceId], [[[UIDevice currentDevice] identifierForVendor] UUIDString]);
}

- (void)testUserIdNotSet {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    [self.corebi flushQueue];
    XCTAssertNil([self.corebi userId]);
}

- (void)testUserIdSet {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey userId:userId];
    [self.corebi flushQueue];
    XCTAssertEqualObjects([self.corebi userId], userId);
}

- (void)testInitializedSet {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    XCTAssert([self.corebi initialized]);
}

- (void)testOptOut {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];

    [self.corebi setOptOut:YES];
    [self.corebi logEvent:@"Opted Out"];
    [self.corebi flushQueue];

    XCTAssert(self.corebi.optOut == YES);
    XCTAssert(![[self.corebi getLastEvent][@"collection"] isEqualToString:@"Opted Out"]);

    [self.corebi setOptOut:NO];
    [self.corebi logEvent:@"Opted In"];
    [self.corebi flushQueue];

    XCTAssert(self.corebi.optOut == NO);
    XCTAssert([[self.corebi getLastEvent][@"collection"] isEqualToString:@"Opted In"]);
}

- (void)testUserPropertiesSet {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];
    XCTAssertEqual([dbHelper getEventCount], 0);

    NSDictionary *properties = @{
         @"shoeSize": @10,
         @"hatSize":  @5.125,
         @"name": @"John"
    };

    [self.corebi setUserProperties:properties];
    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], 0);
    XCTAssertEqual([dbHelper getIdentifyCount], 1);
    XCTAssertEqual([dbHelper getTotalEventCount], 1);

    NSDictionary *expected = [NSDictionary dictionaryWithObject:properties forKey:RKM_OP_SET];

    NSDictionary *event = [self.corebi getLastIdentify];
    XCTAssertEqualObjects([event objectForKey:@"collection"], IDENTIFY_EVENT);
    XCTAssertTrue([self key:[event objectForKey:@"properties"] containsInDictionary:expected]);
}

-(BOOL)key:(NSDictionary *)main containsInDictionary:(NSDictionary *)Dictionary
{
    for (NSString *keyStr in Dictionary) {
        if(![[Dictionary objectForKey:keyStr] isEqual:[main objectForKey:keyStr]]) {
            return false;
        }
    }
    return true;
}

- (void)testSetDeviceId {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];

    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    [self.corebi flushQueue];
    NSString *generatedDeviceId = [self.corebi getDeviceId];
    XCTAssertNotNil(generatedDeviceId);
    XCTAssertEqual(generatedDeviceId.length, 36);
    XCTAssertEqualObjects([dbHelper getValue:@"device_id"], generatedDeviceId);

    // test setting invalid device ids
    [self.corebi setDeviceId:nil];
    [self.corebi flushQueue];
    XCTAssertEqualObjects([self.corebi getDeviceId], generatedDeviceId);
    XCTAssertEqualObjects([dbHelper getValue:@"device_id"], generatedDeviceId);

    id dict = [NSDictionary dictionary];
    [self.corebi setDeviceId:dict];
    [self.corebi flushQueue];
    XCTAssertEqualObjects([self.corebi getDeviceId], generatedDeviceId);
    XCTAssertEqualObjects([dbHelper getValue:@"device_id"], generatedDeviceId);

    [self.corebi setDeviceId:@"e3f5536a141811db40efd6400f1d0a4e"];
    [self.corebi flushQueue];
    XCTAssertEqualObjects([self.corebi getDeviceId], generatedDeviceId);
    XCTAssertEqualObjects([dbHelper getValue:@"device_id"], generatedDeviceId);

    [self.corebi setDeviceId:@"04bab7ee75b9a58d39b8dc54e8851084"];
    [self.corebi flushQueue];
    XCTAssertEqualObjects([self.corebi getDeviceId], generatedDeviceId);
    XCTAssertEqualObjects([dbHelper getValue:@"device_id"], generatedDeviceId);

    NSString *validDeviceId = [CoreBIUtils generateUUID];
    [self.corebi setDeviceId:validDeviceId];
    [self.corebi flushQueue];
    XCTAssertEqualObjects([self.corebi getDeviceId], validDeviceId);
    XCTAssertEqualObjects([dbHelper getValue:@"device_id"], validDeviceId);
}

@end
