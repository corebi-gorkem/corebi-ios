//
//  SessionTests.m
//  SessionTests
//
//  Created by Curtis on 9/24/14.
//  Copyright (c) 2014 CoreBI. All rights reserved.
//
//  NOTE: Having a lot of OCMock partialMockObjects causes tests to be flakey.
//        Combined a lot of tests into one large test so they share a single
//        mockCoreBI object instead creating lots of separate ones.
//        This seems to have fixed the flakiness issue.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import <OCMock/OCMock.h>
#import "CoreBI.h"
#import "CoreBI+Test.h"
#import "BaseTestCase.h"

@interface SessionTests : BaseTestCase

@end

@implementation SessionTests { }

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testSessionAutoStartedBackground {
    // mock application state
    id mockApplication = [OCMockObject niceMockForClass:[UIApplication class]];
    [[[mockApplication stub] andReturn:mockApplication] sharedApplication];
    OCMStub([mockApplication applicationState]).andReturn(UIApplicationStateBackground);

    // mock corebi object and verify enterForeground not called
    id mockCoreBI = [OCMockObject partialMockForObject:self.corebi];
    [[mockCoreBI reject] enterForeground];
    [mockCoreBI initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    [mockCoreBI flushQueueWithQueue:[mockCoreBI initializerQueue]];
    [mockCoreBI flushQueue];
    [mockCoreBI verify];
    XCTAssertEqual([mockCoreBI queuedEventCount], 0);
}

- (void)testSessionAutoStartedInactive {
    id mockApplication = [OCMockObject niceMockForClass:[UIApplication class]];
    [[[mockApplication stub] andReturn:mockApplication] sharedApplication];
    OCMStub([mockApplication applicationState]).andReturn(UIApplicationStateInactive);

    id mockCoreBI = [OCMockObject partialMockForObject:self.corebi];
    [[mockCoreBI expect] enterForeground];
    [mockCoreBI initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey];
    [mockCoreBI flushQueueWithQueue:[mockCoreBI initializerQueue]];
    [mockCoreBI flushQueue];
    [mockCoreBI verify];
    XCTAssertEqual([mockCoreBI queuedEventCount], 0);
}

- (void)testSessionHandling {

    // start new session on initializeApiKey
    id mockCoreBI = [OCMockObject partialMockForObject:self.corebi];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1000];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date)] currentTime];

    [mockCoreBI initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey userId:nil];
    [mockCoreBI flushQueueWithQueue:[mockCoreBI initializerQueue]];
    [mockCoreBI flushQueue];
    XCTAssertEqual([mockCoreBI queuedEventCount], 0);
    XCTAssertEqual([mockCoreBI sessionId], 1000000);

    // also test getSessionId
    XCTAssertEqual([mockCoreBI getSessionId], 1000000);

    // A new session should start on UIApplicationWillEnterForeground after minTimeBetweenSessionsMillis
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date)] currentTime];
    [mockCoreBI enterBackground]; // simulate app entering background
    [mockCoreBI flushQueue];
    XCTAssertEqual([mockCoreBI sessionId], 1000000);

    NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:1000 + (self.corebi.minTimeBetweenSessionsMillis / 1000)];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date2)] currentTime];
    [mockCoreBI enterForeground]; // simulate app entering foreground
    [mockCoreBI flushQueue];

    XCTAssertEqual([mockCoreBI queuedEventCount], 0);
    XCTAssertEqual([mockCoreBI sessionId], 1000000 + self.corebi.minTimeBetweenSessionsMillis);


    // An event should continue the session in the foreground after minTimeBetweenSessionsMillis + 1 seconds
    NSDate *date3 = [NSDate dateWithTimeIntervalSince1970:1000 + (self.corebi.minTimeBetweenSessionsMillis / 1000) + 1];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date3)] currentTime];
    [mockCoreBI logEvent:@"continue_session"];
    [mockCoreBI flushQueue];

    XCTAssertEqual([[mockCoreBI lastEventTime] longLongValue], 1001000 + self.corebi.minTimeBetweenSessionsMillis);
    XCTAssertEqual([mockCoreBI queuedEventCount], 1);
    XCTAssertEqual([mockCoreBI sessionId], 1000000 + self.corebi.minTimeBetweenSessionsMillis);


    // session should continue on UIApplicationWillEnterForeground after minTimeBetweenSessionsMillis - 1 second
    NSDate *date4 = [NSDate dateWithTimeIntervalSince1970:2000];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date4)] currentTime];
    [mockCoreBI enterBackground]; // simulate app entering background

    NSDate *date5 = [NSDate dateWithTimeIntervalSince1970:2000 + (self.corebi.minTimeBetweenSessionsMillis / 1000) - 1];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date5)] currentTime];
    [mockCoreBI enterForeground]; // simulate app entering foreground
    [mockCoreBI flushQueue];

    XCTAssertEqual([mockCoreBI queuedEventCount], 1);
    XCTAssertEqual([mockCoreBI sessionId], 1000000 + self.corebi.minTimeBetweenSessionsMillis);


   // test out of session event
    NSDate *date6 = [NSDate dateWithTimeIntervalSince1970:3000];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date6)] currentTime];
    [mockCoreBI logEvent:@"No Session" withEventProperties:nil outOfSession:NO];
    [mockCoreBI flushQueue];
    XCTAssert([[mockCoreBI getLastEvent][@"properties"][@"session_id"]
               isEqualToNumber:[NSNumber numberWithLongLong:1000000 + self.corebi.minTimeBetweenSessionsMillis]]);

    NSDate *date7 = [NSDate dateWithTimeIntervalSince1970:3001];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date7)] currentTime];
    // An out of session event should have session_id = -1
    [mockCoreBI logEvent:@"No Session" withEventProperties:nil outOfSession:YES];
    [mockCoreBI flushQueue];
    XCTAssert([[mockCoreBI getLastEvent][@"properties"][@"session_id"]
               isEqualToNumber:[NSNumber numberWithLongLong:-1]]);

    // An out of session event should not continue the session
    XCTAssertEqual([[mockCoreBI lastEventTime] longLongValue], 3000000); // event time of first no session
}

- (void)testEnterBackgroundDoesNotTrackEvent {
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : apiKey userId:nil];
    [self.corebi flushQueueWithQueue:self.corebi.initializerQueue];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:UIApplicationDidEnterBackgroundNotification object:nil userInfo:nil];

    [self.corebi flushQueue];
    XCTAssertEqual([self.corebi queuedEventCount], 0);
}

- (void)testTrackSessionEvents {
    id mockCoreBI = [OCMockObject partialMockForObject:self.corebi];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1000];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date)] currentTime];
    [mockCoreBI setTrackingSessionEvents:YES];

    [mockCoreBI initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey userId:nil];
    [mockCoreBI flushQueueWithQueue:[mockCoreBI initializerQueue]];
    [mockCoreBI flushQueue];

    XCTAssertEqual([mockCoreBI queuedEventCount], 1);
    XCTAssertEqual([[mockCoreBI getLastEvent][@"properties"][@"session_id"] longLongValue], 1000000);
    XCTAssertEqualObjects([mockCoreBI getLastEvent][@"collection"], kRKMSessionStartEvent);


    // test end session with tracking session events
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date)] currentTime];
    [mockCoreBI enterBackground]; // simulate app entering background

    NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:1000 + (self.corebi.minTimeBetweenSessionsMillis / 1000)];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date2)] currentTime];
    [mockCoreBI enterForeground]; // simulate app entering foreground
    [mockCoreBI flushQueue];
    XCTAssertEqual([mockCoreBI queuedEventCount], 3);

    long long expectedSessionId = 1000000 + self.corebi.minTimeBetweenSessionsMillis;
    XCTAssertEqual([mockCoreBI sessionId], expectedSessionId);

    XCTAssertEqual([[self.corebi getEvent:1][@"properties"][@"session_id"] longLongValue], 1000000);
    XCTAssertEqualObjects([self.corebi getEvent:1][@"collection"], kRKMSessionEndEvent);
    XCTAssertEqual([[self.corebi getEvent:1][@"properties"][@"_time"] longLongValue], 1000000);

    XCTAssertEqual([[self.corebi getLastEvent][@"properties"][@"session_id"] longLongValue], expectedSessionId);
    XCTAssertEqualObjects([self.corebi getLastEvent][@"collection"], kRKMSessionStartEvent);

    // test in session identify with app in background
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date2)] currentTime];
    [mockCoreBI enterBackground]; // simulate app entering background

    NSDate *date3 = [NSDate dateWithTimeIntervalSince1970:1000 + 2 * self.corebi.minTimeBetweenSessionsMillis];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date3)] currentTime];
    CoreBIIdentify *identify = [[CoreBIIdentify identify] set:@"key" value:@"value"];
    [mockCoreBI identify:identify outOfSession:NO];
    [mockCoreBI flushQueue];
    XCTAssertEqual([mockCoreBI queuedEventCount], 5); // triggers session events

    // test out of session identify with app in background
    NSDate *date4 = [NSDate dateWithTimeIntervalSince1970:1000 + 3 * self.corebi.minTimeBetweenSessionsMillis];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date4)] currentTime];
    [mockCoreBI identify:identify outOfSession:YES];
    [mockCoreBI flushQueue];
    XCTAssertEqual([mockCoreBI queuedEventCount], 5); // does not trigger session events
}

- (void)testSessionEventsOn32BitDevices {
    id mockCoreBI = [OCMockObject partialMockForObject:self.corebi];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:21474836470];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date)] currentTime];
    [mockCoreBI setTrackingSessionEvents:YES];

    [mockCoreBI initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] :apiKey userId:nil];
    [mockCoreBI flushQueueWithQueue:[mockCoreBI initializerQueue]];
    [mockCoreBI flushQueue];

    XCTAssertEqual([mockCoreBI queuedEventCount], 1);
    XCTAssertEqual([[mockCoreBI getLastEvent][@"properties"][@"session_id"] longLongValue], 21474836470000);
    XCTAssertEqualObjects([mockCoreBI getLastEvent][@"collection"], kRKMSessionStartEvent);


    // test end session with tracking session events
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date)] currentTime];
    [mockCoreBI enterBackground]; // simulate app entering background

    NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:214748364700];
    [[[mockCoreBI expect] andReturnValue:OCMOCK_VALUE(date2)] currentTime];
    [mockCoreBI enterForeground]; // simulate app entering foreground
    [self.corebi flushQueue];
    XCTAssertEqual([mockCoreBI queuedEventCount], 3);

    XCTAssertEqual([mockCoreBI sessionId], 214748364700000);

    XCTAssertEqual([[self.corebi getEvent:1][@"properties"][@"session_id"] longLongValue], 21474836470000);
    XCTAssertEqualObjects([self.corebi getEvent:1][@"collection"], kRKMSessionEndEvent);

    XCTAssertEqual([[self.corebi getLastEvent][@"properties"][@"session_id"] longLongValue], 214748364700000);
    XCTAssertEqualObjects([self.corebi getLastEvent][@"collection"], kRKMSessionStartEvent);
}

- (void)testSkipSessionCheckWhenLoggingSessionEvents {
    CoreBIDatabaseHelper *dbHelper = [CoreBIDatabaseHelper getDatabaseHelper];

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1000];
    NSNumber *timestamp = [NSNumber numberWithLongLong:[date timeIntervalSince1970] * 1000];
    [dbHelper insertOrReplaceKeyLongValue:@"previous_session_id" value:timestamp];

    self.corebi.trackingSessionEvents = YES;
    [self.corebi initializeApiKey:[NSURL URLWithString:@"http://127.0.0.1:9998"] : apiKey userId:nil];

    [self.corebi flushQueue];
    XCTAssertEqual([dbHelper getEventCount], 2);
    NSArray *events = [dbHelper getEvents:-1 limit:2];
    XCTAssertEqualObjects(events[0][@"collection"], kRKMSessionEndEvent);
    XCTAssertEqualObjects(events[1][@"collection"], kRKMSessionStartEvent);
}

@end
