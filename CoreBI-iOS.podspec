Pod::Spec.new do |s|
  s.name                   = "CoreBI-iOS"
  s.version                = "4.0.1"
  s.summary                = "CoreBI mobile analytics iOS SDK."
  s.homepage               = "https://corebi.io"
  s.license                = { :type => "MIT" }
  s.author                 = { "CoreBI" => "emre@corebi.io" }
  s.source                 = { :git => "https://github.com/corebi-io/corebi-ios.git", :tag => "v4.0.1" }
  s.ios.deployment_target  = '6.0'
  s.tvos.deployment_target = '9.0'
  s.source_files           = 'CoreBI/*.{h,m}'
  s.requires_arc           = true
  s.library 	           = 'sqlite3.0'
end
