//
//  CoreBIURLConnection.h
//  CoreBI
//
//  Copyright (c) 2015 CoreBI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreBIURLConnection : NSObject <NSURLConnectionDelegate,NSURLConnectionDataDelegate>

+ (void)sendAsynchronousRequest:(NSURLRequest *)request queue:(NSOperationQueue *)queue completionHandler:(void (^)(NSURLResponse *response, NSData *data, NSError *connectionError))handler;

@end